#ifndef CMAP_H
#define CMAP_H

#include <map>
#include <string>
#include <thread>
#include <mutex>
#include <iostream>
#include <chrono>
#include <sstream>

/**
 *  namespace   Пространство имен класса CMap
 */
namespace SafeMap {

/// thread::id по умолчанию
static std::thread::id DEFAULT_THREAD_ID;

/**
 * @brief   Характеристики записей мапа
 */
struct ItemInfo {
    std::shared_ptr<std::recursive_mutex> m_mutex;    // мьютекс
    std::thread::id  m_threadId;            // ID потока, работающего с записью
    bool m_isExclusive;   // признак получения эксклюзивного доступа
    ItemInfo();
} ;

/**
 * @brief   Класс CMap. Потокобезопасный map.
 */
class CMap
{
public:

    CMap();     // конструктор по умолчанию
    ~CMap();    // деструктор

    /// получение эксклюзивного доступа к записи
    void acquireExclusive(const std::string& key);

    /// освобождение эксклюзивного доступа к записи
    void releaseExclusive(const std::string& key);

    /// прочитать запись
    const std::string& readField(const std::string& key);

    /// изменить запись
    void writeField(const std::string& key, const std::string& value);

    /// удалить запись
    void removeField(const std::string& key);

    void print();

    std::thread::id getDefaultThreadId() const; // вернуть ID текущего потока

private:
    CMap(CMap&);            // конструктор копирования
    CMap& operator=(CMap&); // оператор копирования

    std::map<std::string, std::string>m_valuesMap;  // мап текущих записей
    std::map<std::string, ItemInfo> m_threadsMap; // мап потоков с эксклюзивными правами

    void print(const std::string& key); // вывести запись на экран

    const std::thread::id  m_defaultId; // ID потока по умолчанию

};

} // namespace SafeMap
#endif // CMAP_H
