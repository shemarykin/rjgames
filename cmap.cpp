#include "cmap.h"

using namespace std;
using namespace SafeMap;

/**
 * @brief   Конструктор.
 */
CMap::CMap()
: m_defaultId(this_thread::get_id())
{
    DEFAULT_THREAD_ID = m_defaultId;
}

/**
 * @brief   Деструктор.
 * @details Освобождаем ресурсы.
 */
CMap::~CMap()
{
    m_valuesMap.clear();
    m_threadsMap.clear();
    cout << "Clean..." << endl;
}

/**
 * @brief   Получить эксклюзивный доступ к записи.
 * @details При запросе получения доступа, по %thread_id проверяется
 * возможность доступа. Если доступ к записи уже занят другим потоком, то
 * данный поток будет ждать (уснет), т.к. доступ блокирующий. Если не занят - то
 * доступ будет предоставлен.
 * @param   key         Ключ записи.
 */
void CMap::acquireExclusive(const string& key)
{
    m_threadsMap[key].m_mutex.get()->lock();

    const thread::id thread_id = this_thread::get_id();
    m_threadsMap[key].m_threadId = thread_id;
    m_threadsMap[key].m_isExclusive = true;
    //cout << "   [X] locked   [" << key << "]\t[#" << thread_id << "]" << endl;
}

/**
 * @brief   Отменить эксклюзивный доступ.
 * @param   key         Ключ записи.
 */
void CMap::releaseExclusive(const string& key)
{
    const thread::id thread_id = this_thread::get_id();
    if (m_threadsMap[key].m_threadId == thread_id
            && m_threadsMap[key].m_isExclusive) {
        m_threadsMap[key].m_threadId = m_defaultId;
        m_threadsMap[key].m_isExclusive = false;
        m_threadsMap[key].m_mutex.get()->unlock();
        //cout << "   [ ] UNLOCKED [" << key << "]\t[#" << thread_id << "]" << endl;
    }
}

/**
 * @brief   Прочитать запись с ключом %key.
 * @param   key         Ключ записи.
 * @return  Значение записи.
 */
const string& CMap::readField(const string& key)
{
    //print(key);
    return m_valuesMap[key];
}

/**
 * @brief   Сделать запись с ключом %key.
 * @param   key         Ключ записи.
 * @param   value       Новое значение записи.
 */
void CMap::writeField(const string& key, const string& value)
{
    // если поток в списке привелегированных, пишем
    const thread::id thread_id = this_thread::get_id();
    if (m_threadsMap[key].m_threadId == thread_id
            && m_threadsMap[key].m_isExclusive) {
        m_valuesMap[key] = value;
        //cout << "Key [" << key << "] changed to " << value << endl;
        //releaseExclusive(thread_id, key);
    }
}

/**
 * @brief   Удалить запись с ключом %key.
 * @param   key         Ключ записи.
 */
void CMap::removeField(const string &key)
{
    // если поток в списке привелегированных, удаляем
    const thread::id thread_id = this_thread::get_id();
    if (m_threadsMap[key].m_threadId == thread_id
            && m_threadsMap[key].m_isExclusive) {
        m_valuesMap.erase(m_valuesMap.find(key));
    }
}

/**
 * @brief   Вывести все записи.
 */
void CMap::print()
{
    for (const auto& v : m_valuesMap) {
        cout << "map[" << v.first << "] " << v.second << endl;
    }
}

/**
 * @brief   Напечатать запись с ключом %key.
 * @param   key         Ключ записи.
 */
void CMap::print(const string& key)
{
    const thread::id thread_id = this_thread::get_id();
    cout << "\t#" << thread_id;
    cout << "\t" << key << " : " << m_valuesMap[key] << endl;
    cout << ".";
}

/**
 * @brief   Вернуть текущий ID потока.
 * @return  ID потока.
 */
std::thread::id CMap::getDefaultThreadId() const
{
    return m_defaultId;
}

/**
 * @brief   Конструктор.
 */
ItemInfo::ItemInfo()
    : m_mutex(make_shared<recursive_mutex>())
    , m_threadId(DEFAULT_THREAD_ID)
    , m_isExclusive(false)
{
}
