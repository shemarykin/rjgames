/*
 * Спроектировать и реализовать потокобезопасный мап со следующими свойствами:
 *
 *
 * 1.   Ключом и значением является строка.
 *
 * 2.   Потоки могут получать эксклюзивный доступ к записям в мапе.
 *
 * 3.   Только поток, получивший эксклюзивный доступ к записи может менять ее
 *      значение.
 *
 * 4.   Один поток может получить эксклюзивный доступ к нескольким записям.
 *
 * 5.   Если потоку нужно обратиться к записи, которая в эксклюзивном доступе у
 *      другого потока, то он должен ждать пока запись освободится.
 *
 * 6.   Если запрашивается доступ к записи, которой нет в мапе, то создается и
 *      возвращается новая запись.
 */


#include "cmap.h"
#include <ctime>
#include <vector>
#include <random>

using namespace std;
using SafeMap::CMap;

template<typename T>
string toString(T value)
{
    ostringstream oss;
    oss << value;
    return oss.str();
}

int main()
{
    std::srand(std::time(0));

    /// мап

    CMap map;
    cout << map.getDefaultThreadId() << endl;

    // создаем тестовые записи
    constexpr int TEST_FILELDS{100};
    for (int i=0; i<TEST_FILELDS; ++i) {
        string key{"key-" + toString(i)};
        string value{"value-" + toString(i)};
        map.acquireExclusive(key);
        map.writeField(key, value);
        //cout << "[" << key << "] " << map.readField(key) << endl;
        map.releaseExclusive(key);
    }
    map.print();


    /// потоки

    constexpr int THREADS_NUM{10};
    vector<thread> threads;
    for (int i=0; i<THREADS_NUM; ++i)
    {
        threads.push_back(thread([&map]()
        {
            thread::id id = this_thread::get_id();
            int min=10, max=20;
            int acquire = rand() % max + min;
            int release = rand() % max + acquire;
            if (release > 2*max) release = 2*max;

            string key_rand{"key-" + toString(rand() % TEST_FILELDS)};
            string value{"LEGAL-" + toString(rand() % TEST_FILELDS) +
                        " " + toString(id)};

            // в случайное время захватываем и освобождаем доступ
            for (int i = min; i <= 2*max; ++i)
            {
                if (i == acquire) {
                    map.acquireExclusive(key_rand);
                }
                if (i == release) {
                    map.releaseExclusive(key_rand);
                }

                // пытаемся читать и записывать записи
                for (int j=0; j<TEST_FILELDS/2; ++j) {
                    string key{"key-" + toString(rand() % TEST_FILELDS)};

                    string val{"LEGAL-" + toString(rand() % TEST_FILELDS) +
                                " " + toString(id)};
                    // пробуем изменить запись там, где нет доступа
                    if (key != key_rand) {
                        val = "ERROR-" + toString(rand() % TEST_FILELDS) +
                                " " + toString(id);
                    }
                    map.writeField(key, val);
                    string read = map.readField(key);
                }

                this_thread::sleep_for(chrono::milliseconds(1));
            }
        })); // push_back

    }
    for (auto& t : threads) {
        if (t.joinable())
            t.join();
    }
    cout << "~ END ~" << endl;

    map.print();
}
